<!-- LTeX: language=fr -->
# VERSION D'UNE ANNEE PASSEE GARDEE SEULEMENT POUR MEMOIRE Avancée / Planning du cours de MIF08 (Compilation)
_Année 2023-2024_

~

~

~

# CETTE PAGE EST UNE ANCIENNE VERSION GARDÉE SEULEMENT POUR MÉMOIRE

La page de l'année 2025 est ici : https://forge.univ-lyon1.fr/matthieu.moy/mif08-2025/

# 
~

~

~

* Matthieu Moy, Université Lyon 1, LIP https://matthieu-moy.fr/

## Communication et nouvelles du cours

* [NEWS.md](NEWS.md) contient les nouvelles du cours (envoyées par email également).

* Cette année, nous ~~utiliserons~~ détournerons le système d'issues de GitLab
  comme forum minimaliste. Vous pouvez poser vos questions en ajoutant une issue
  sur le dépôt : https://forge.univ-lyon1.fr/matthieu.moy/mif08-2024/-/issues .
  Activez les notifications sur le projet GitLab pour avoir un mail à chaque
  nouveau post.

## Intervenants

**CM**

Matthieu Moy

**TD**
- A: Matthieu Moy (32 étudiants)
- B: Alec Sadler (32 étudiants)
- C: Gregoire Pichon (33 étudiants)
- D: Hugo Thievenaz (32 étudiants)

**TP**
- A1: Matthieu Moy
- A2: Qi Qiu
- B1: Alec Sadler
- B2: Nicolas Louvet
- C1: Gregoire Pichon
- C2: Thais Baudon
- D1: Thibaut Modrzyk
- D2: Hugo Thievenaz

## Vidéos des CM

Les vidéos réalisées pendant le COVID sont encore disponibles. Elles ne sont plus très à jour, mais peuvent vous aider si besoin (elles ne remplacent pas le présentiel !) :

[La playlist Youtube MIF08](https://www.youtube.com/playlist?list=PLtjm-n_Ts-J9HSZ9ahpbsC_kTQMzUZQPx)

## Infrastructure technique, logiciels à installer

Les TP utilisent la chaîne d'outils RiscV, un peu lourde à installer. Voir [INSTALL.md](INSTALL.md) pour les consignes. À faire avant les TPs si vous voulez travailler sur vos machines personnelles.

Si vous n'arrivez pas à installer les outils sur vos machines, vous pourrez travailler sur les ordinateurs de la fac, et en dernier recours nous fournissons aussi des machines virtuelles pré-installées : [VM.md](VM.md).

## Planning

## Jeudi 15/02/2024

- :book: 8h: Cours 1: Introduction, machine cible (RISCV), lexing :
    - Introduction au cours, à la compilation et à l'architecture cible : [transparents](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/capmif_cours01_intro_et_archi.pdf)
    - Lexing (et parsing) : [transparents](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/capmif_cours02_lexing_parsing.pdf) <!-- Lexing nécessaire pour TD1, parsing si possible -->
    - [Vidéo "teaser"](https://youtu.be/ny7HlqyuM9E)
    - [vidéo d'introduction au cours](https://www.youtube.com/watch?v=zGifE8MfPWA)
    - [vidéo sur RISCV](https://youtu.be/ZdElX9e_tAI)
    - [vidéo lexing](https://www.youtube.com/watch?v=UlUTSsOA9Qc)
    - Extrait de la documentation RISCV: [riscv_isa.pdf](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/riscv_isa.pdf)
    - :100: QCM sur TOMUSS, à faire avant mardi 20/2/2024, 23:59

- :pencil2: 9h45: TD1 : Architecture RISCV, Lexing, Parsing
    - [Énoncé du TD1](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/td1.pdf)
    - Rappel, extrait de la documentation RISCV : [riscv_isa.pdf](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/riscv_isa.pdf)

- :hammer: 11h30: TP1 : Python et RiscV
    - Énoncé : [TP1 python/archi](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/tp1.pdf)
    - Fichiers du TP1 : [TP01/](TP01/).

## Jeudi 22/2/2024

- :book: 9h45: Cours 2: Parsing, interprétation
    - Parsing : [deuxième partie des transparents](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/capmif_cours02_lexing_parsing.pdf)
    - [vidéo parsing](https://www.youtube.com/watch?v=y9MrfDzrAmA)
    - [transparents sémantique et interprète](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/capmif_cours03_interpreters.pdf)
    - [vidéo sémantique et interprète](https://youtu.be/8PYhBsgRO6g)

- :100: QCM sur TOMUSS, à faire avant lundi 11 mars 2024, 23:59.

- :pencil2: 11h30: TD, Arbres abstraits, attributions
    - Contrôle court, voir instructions : [QCM.md](QCM.md)
    - [Énoncé du TD2](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/td2.pdf)

## Jeudi 14/3/2024

- :hammer: 8h-11h15 : TP2, ANTLR
    - Transparents de présentation : [capmif_labs.pdf](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/capmif_labs.pdf)
    - Si besoin : des VM pour vous dépanner en cas de problème sur machine perso : [VM.md](VM.md)
    - Énoncé : [TP2 antlr](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/tp2.pdf)
    - Fichiers du TP2 : [TP02/](TP02/).
    - **Date limite pour le rendu (noté) : mercredi 20 mars 2024, 23h59. (deadline stricte)**

## Jeudi 21/3/2024

- :book: Cours 3, Typage : 8h-9h30
    - [transparents typage](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/capmif_cours04_typing.pdf)
    - [vidéo typage](https://youtu.be/2A-hQy_6YlE)
- :100: QCM sur TOMUSS, à faire avant lundi 8 avril 2024, 23:59.

- :hammer: TP3, interprète MiniC : 9h45-13h
    - TP noté (20 minutes). Sujet pour s'entraîner : [CC2/cc2-sujet-exemple.pdf](CC2/cc2-sujet-exemple.pdf) (squelette et corrigé dans le même répertoire).
    - Énoncé : [TP3 frontend, interprète](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/tp3.pdf)
    - Fichiers du TP3 : [TP03/](TP03/) puis [MiniC/](MiniC/).
    - Date de rendu fortement recommandée pour le TP3 : mercredi 10 avril 2024 (pour pouvoir démarrer le TP4 le jeudi 11 avril 2024). Pour tenir compte du délai court entre TP3 et TP4 **date limite de rendu du TP3 : dimanche 14 avril 2024, 23h59 (deadline stricte).**

## Jeudi 11/4/2024

- :book: Cours 4 : 8h-9h30
    - génération de code 3 adresses + allocation naïve, [transparents](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/capmif_cours05_3ad_codegen.pdf), [vidéo](https://youtu.be/m2x7leFnCN4)
    - Représentations intermédiaires, [transparents](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/capmif_cours06_irs.pdf), [vidéo 6a](https://youtu.be/dD9bRhLfykM), [vidéo 6b](https://youtu.be/Xico_JTK3XQ).

- :100: QCM sur TOMUSS, à faire avant mardi 16 avril 2024, 23:59.

- :pencil2: TD 3, génération de code : 9h45-11h15
    - Sujet : [TD3 typage et génération de code](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/td3.pdf)
    - [Éléments de corrigé, partiel](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/td3-corrige-partiel.pdf)

- :hammer: TP 4, génération de code : 11h30-13h
    - Transparents de présentation : [capmif_labs.pdf](https://compil-lyon.gitlabpages.inria.fr/mif08-20/capmif_labs.pdf)
    - Rappel : des VM pour vous dépanner en cas de problème sur machine perso : [VM.md](VM.md)
    - Énoncé : [TP4 génération de code](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/tp4.pdf)
    - Fichiers du TP4 : [MiniC/TP04/](MiniC/TP04/).
    - [Documentation de la bibliothèque fournie](http://matthieu.moy.pages.univ-lyon1.fr/mif08-2024/)
    - **Date limite pour le rendu (noté) : mardi 7 mai 2024, 23h59 (malus 2 points par jour de retard, aucun rendu accepté après le dimanche soir).**

## Jeudi 18/4/2024

- :hammer: TP 4 (suite), 8h-11h15 : cf. ci-dessus pour les supports.

## Jeudi 16/5/2024

- :book: Cours 5, allocation de registres : 8h-9h30
    - Register allocation + data-flow analyses : [transparents](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/capmif_cours07_regalloc.pdf), [vidéo première partie](https://youtu.be/9902mMgDIK8), [vidéo deuxième partie](https://youtu.be/LknSDccweFw).
    - :100: QCM sur TOMUSS, à faire avant mardi 4 juin 2024, 23:59.


- :hammer: TP5, nouvelles fonctionnalités de langage : 9h45-13h
    - Énoncé : [TP5 (extension de langage)](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/tp5for.pdf)
    - Fichiers du TP5 : les mêmes qu'aux TP précédents.
     - **Date limite pour le rendu (noté) : vendredi 7 juin 2024, 23h59 (malus 2 points par jour de retard, aucun rendu accepté après le dimanche soir).**

## Jeudi 6/6/2024

- :pencil2: TD4, analyse de vivacité : 9h45-11h15
    - Énoncé : [TD4 liveness](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/td4.pdf)


- :pencil2: TD5, allocation de registres intelligente : 11h30-13h
    - Énoncé : [TD5 regalloc](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/td5.pdf)

## Jeudi 13/06/2024

- :100: Examen.

## Pondération des notes (indicative pour l'instant sauf l'examen final qui sera forcément 40%)
  - QCM : non pris en compte dans la moyenne d'UE
  - 2 ou 3 interrogations écrites : 20% au total
  - TP2 parsing et évaluation d'expression : 6%
  - TP3 interprète : 10%
  - TP4 génération de code : 12%
  - TP5 extension de langage : 12%
  - Contrôle terminal (CT) : 40 %

Pour les interrogations écrites, la note prise en compte sera le maximum entre la note obtenue et la note de contrôle terminal. En cas d'absence (justifiée ou non), la note de CT remplace la note d'interrogation.

La session 2 remplace la note de contrôle terminal.

## Annales et consignes pour l'examen

* Aide-mémoire fourni avec le sujet en 2021 (un document similaire sera fourni cette année) : [mif08_companion.pdf](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/mif08_companion.pdf)

* L'examen session 1 2022-2023 : [exam_mif08_2023.pdf](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/exam_mif08_2023.pdf) et éléments de corrigé : [exam_mif08_2023_corr.pdf](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/exam_mif08_2023_corr.pdf) 

* L'examen session 1 2021-2022 : [exam_mif08_2022.pdf](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/exam_mif08_2022.pdf) et éléments de corrigé : [exam_mif08_2022_corr.pdf](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/exam_mif08_2022_corr.pdf)

* L'examen Session 1 2020-2021 : [exam_mif08_2020.pdf](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/exam_mif08_2020.pdf) et les éléments de corrigé : [exam_mif08_2020_corr.pdf](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/exam_mif08_2020_corr.pdf)

* L'examen 2019-2020 : [mif08_exam1920.pdf](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/mif08_exam1920.pdf) et les éléments de corrigés : [mif08_exam1920-corr.pdf](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/mif08_exam1920-corr.pdf).

* [Consignes pour l'examen](https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/exam_mif08-page1.pdf)
