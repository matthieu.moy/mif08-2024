<!-- LTeX: language=fr -->
# Nouvelles du cours

Les nouvelles du cours apparaîtront ici au fur et à mesure.

## 16/05/2024 : précision sur continue, for et while

J'ai précisé un point dans l'énoncé du TP5 : l'instruction 'continue' est
limitée aux boucles for (on ne peut pas l'utiliser dans une boucle while, elle
redémarre la boucle for la plus imbriquée en ignorant les boucles while).

Ce n'est pas ce que fait le vrai C, mais ceci devrait vous simplifier un peu la
tâche pour coder.

## 20/04/2024 : informations et conseils sur les TP (3 et 4) et TD 3

Quelques informations et conseils en vrac :

* J'ai eu beaucoup de questions sur le barème du TP3, en particulier pour la
  couverture de votre base de tests. Pour évaluer la couverture de votre base de
  tests, j'utilise *votre* base de tests sur *mon* compilateur. Vous pouvez tout
  à fait avoir 100% de couverture chez vous si vous avez complètement oublié
  quelque chose (par exemple, vous n'avez aucun test sur modulo avec opérandes
  négatifs, vous avez probablement un bug dû à la gestion du modulo différente
  en C et en Python, mais vous avez 100% de couverture chez vous. Chez moi par
  contre, il manquera les cas particuliers du code Python de mon interprète).
  J'ai une échelle de notation non-linéaire : les premières lignes de codes
  couvertes sont assez faciles à avoir, et rapportent moins de points (si
  l'énoncé était « écrivez un programme avec un while, un avec un else, un autre
  avec un modulo et encore un autre avec une division », ce serait un exercice
  de L1, pas une évaluation de votre démarche de tests au niveau master ...).
  Une base de tests qui couvre la moitié des lignes non-couvertes par le
  squelette n'a qui le quart des points.

  Ah, et bien sûr, pour ceux qui ont lancé leur base de tests sans lire ce
  qu'elle affichait, j'insiste sur la petite phrase « Coverage HTML written to
  dir htmlcov » qui vous permet d'accéder au rapport de couverture détaillé ...

* Pour le TP4, je lancerai les tests contenus dans le répertoire `TP04/` pour
  évaluer votre couverture. Si vous avez des tests pertinents dans `TP03/` que
  vous souhaitez reprendre pour le TP4, vous pouvez les recopier ou faire des
  liens symboliques.

* Je vous encourage vivement à utiliser le principe du Test Driven Development
  (TDD), présenté en MIF01, pour vos TPs. Pour ajouter une fonctionnalité à
  votre compilateur, faites dans l'ordre :

  1. Ajouter un (ou plusieurs tests)
  2. Vérifier qu'il échoue, pour les bonnes raisons (ici, vous devez avoir un
     `test_expect` qui passe et le test correspondant de votre compilateur qui
     échoue)
  3. Maintenant seulement, vous vous autorisez à écrire du Python, en écrivant
     le code minimal qui permet de faire passer le test. Pour gagner un peu de
     temps, vous pouvez utiliser `make PYTEST_OPTS=-x test` qui arrête la base
     de tests au premier test qui échoue.
  4. Une fois que les tests passent, vous pouvez refactorer si besoin, en
     vérifiant que les tests passent toujours, pour avoir un code le plus
     élégant possible.
  5. `git commit`, retour au point 1 avec une autre fonctionnalité.

  Avec cette méthode, par construction, vous n'avez jamais de code non-testé (la
  couverture vous permet de le vérifier, mais ça devrait toujours rester 100%).
  Ce n'est pas toujours simple à mettre en place dans la vraie vie, mais ces TPs
  où les tests sont simples à écrire et l'infrastructure de tests fournie sont
  un très bon endroit faire du TDD. Vous verrez que vous coderez plus vite et
  avec moins de bugs en travaillant comme cela.

* Il y aura un contrôle court en TD le 6 juin.

* Des éléments de corrigé sur une partie du TD3 sont en ligne ici :
  https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/td3-corrige-partiel.pdf

## 14/03/2024 : précisions sur associativité et priorité en ANTLR

J'ai ajouté quelques précisions sur la gestion de l'associativité et des
priorités des opérateurs avec ANTLR dans les transparents de cours « lexing,
parsing » :

https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/capmif_cours02_lexing_parsing.pdf

Slides 15/36 (Lexer rules : dealing with ambiguities) et 32/36 (Parser rules :
dealing with ambiguities).

## 5/03/2024 : TP noté le 21 mars

En début de séance de TP le 21 mars, nous aurons une courte épreuve de TP (20
minutes). Vous avez un sujet « pour s'entraîner » sur la page du cours,
[CC2/cc2-sujet-exemple.pdf](CC2/cc2-sujet-exemple.pdf). Le sujet le jour J sera
très similaire, si vous avez fait ce sujet d'entraînement le TP noté devrait
être assez facile.

## 19/02/2024 : QCM noté ce jeudi

Comme annoncé en CM, vous aurez un examen court, de type QCM, noté, en début de
TD à 11h30 ce jeudi (durée environ 15 minutes). Les consignes détaillées sont
ici :
[QCM.md](https://forge.univ-lyon1.fr/matthieu.moy/mif08-2024/-/blob/main/QCM.md?ref_type=heads).
N'oubliez pas d'apporter votre documentation RiscV, et arrivez à l'heure.

## 15/02/2024 : VM réparées, QCM, fin du TP

J'avais fait plusieurs bêtises à propos des VM (à utiliser en dernier recours si
vous ne pouvez utiliser ni le .tar.gz ni Docker), normalement tout est réparé.
Les IP des VM ont changées (cf. [VM.md](VM.md)).

Je rappelle que vous avez un QCM à faire sur TOMUSS (deadlines indiquées sur la
page d'accueil, [README.md](README.md)), la note ne compte pas dans la moyenne,
mais je prévois un petit contrôle en TD la semaine prochaine qui sera très
inspiré du QCM (et qui lui donnera une note comptée dans la moyenne) ...

Très peu d'entre vous ont fait l'ensemble du TP ce matin. C'est normal, le sujet
est un peu long. Assurez-vous que vous êtes à l'aise avec Python. Vous n'avez
pas forcément besoin de faire toutes les questions Python du TP, mais vérifiez
que vous êtes à l'aise avec les notions, y compris le typage statique avec
Pyright en fin de TP. C'est aussi indispensable que vous soyez capables
d'assembler et exécuter des programmes RiscV (sur vos machines ou celles de la
fac), sinon vous perdrez beaucoup de temps à installer tout ça quand on arrivera
à la génération de code plus tard dans le semestre.
